#include <Arduino.h>
#include "dataFeedlog.pb.h"

#include "pb_common.h"
#include "pb.h"
#include "pb_encode.h"

 struct testdata{
    uint64_t timestamp;
    uint32_t amount;
    uint32_t trigger;
    uint32_t pond_id;
 };

extern "C" void app_main()
{
    initArduino();
    Serial.begin(115200);

    uint8_t buffer[128];
    // struct testdata *data;
    DATA datafeed = {};

    pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));
    
    JSONmsg message = JSONmsg_init_zero;
    // JSONmsg_DATA datafeed = JSONmsg_DATA_init_zero;

    message.success = true;
    message.data_count = 100;
    message.data = {0,0,0,0};

    DATA _data = DATA_init_zero;
    _data.amount = 34;
    _data.pond_id = 123123;
    _data.timestamp = 1231321231;
    _data.trigger = 3;

    message.data = _data;



    // data = {
    //     datafeed.timestamp = 1578182400;
    //     amount = 1250000,
    //     trigger = 3,
    //     pond_id = 1234567,
    // };

    bool status = pb_encode(&stream, JSONmsg_fields, &message);

    if (!status)
    {
        Serial.println("Failed to encode");
        return;
    }

    Serial.print("Message Length: ");
    Serial.println(stream.bytes_written);

    Serial.print("Message: ");

    for (int i = 0; i < stream.bytes_written; i++)
    {
        Serial.printf("%02X", buffer[i]);
    }
}