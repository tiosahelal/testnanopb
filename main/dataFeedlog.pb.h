/* Automatically generated nanopb header */
/* Generated by nanopb-0.4.2 */

#ifndef PB_DATAFEEDLOG_PB_H_INCLUDED
#define PB_DATAFEEDLOG_PB_H_INCLUDED
#include <pb.h>

#if PB_PROTO_HEADER_VERSION != 40
#error Regenerate this file with the current version of nanopb generator.
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Struct definitions */
typedef struct _DATA {
    uint64_t timestamp;
    uint32_t amount;
    uint32_t trigger;
    uint32_t pond_id;
} DATA;

typedef struct _JSONmsg {
    bool success;
    uint32_t data_count;
    pb_callback_t data;
} JSONmsg;


/* Initializer values for message structs */
#define DATA_init_default                        {0, 0, 0, 0}
#define JSONmsg_init_default                     {0, 0, {{NULL}, NULL}}
#define DATA_init_zero                           {0, 0, 0, 0}
#define JSONmsg_init_zero                        {0, 0, {{NULL}, NULL}}

/* Field tags (for use in manual encoding/decoding) */
#define DATA_timestamp_tag                       1
#define DATA_amount_tag                          2
#define DATA_trigger_tag                         3
#define DATA_pond_id_tag                         4
#define JSONmsg_success_tag                      1
#define JSONmsg_data_count_tag                   2
#define JSONmsg_data_tag                         3

/* Struct field encoding specification for nanopb */
#define DATA_FIELDLIST(X, a) \
X(a, STATIC,   REQUIRED, UINT64,   timestamp,         1) \
X(a, STATIC,   REQUIRED, UINT32,   amount,            2) \
X(a, STATIC,   REQUIRED, UINT32,   trigger,           3) \
X(a, STATIC,   REQUIRED, UINT32,   pond_id,           4)
#define DATA_CALLBACK NULL
#define DATA_DEFAULT NULL

#define JSONmsg_FIELDLIST(X, a) \
X(a, STATIC,   REQUIRED, BOOL,     success,           1) \
X(a, STATIC,   REQUIRED, UINT32,   data_count,        2) \
X(a, CALLBACK, REPEATED, MESSAGE,  data,              3)
#define JSONmsg_CALLBACK pb_default_field_callback
#define JSONmsg_DEFAULT NULL
#define JSONmsg_data_MSGTYPE DATA

extern const pb_msgdesc_t DATA_msg;
extern const pb_msgdesc_t JSONmsg_msg;

/* Defines for backwards compatibility with code written before nanopb-0.4.0 */
#define DATA_fields &DATA_msg
#define JSONmsg_fields &JSONmsg_msg

/* Maximum encoded size of messages (where known) */
#define DATA_size                                29
/* JSONmsg_size depends on runtime parameters */

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
